#!/bin/bash

FILE=transcode-to-wav
NEMO_PATH="$HOME/.local/share/nemo/scripts"

function check_if_installed()
{
	PACKAGE_NAME=$1
	RESULT=`dpkg -l $PACKAGE_NAME | grep ii -c`
	if [ "$RESULT" -eq 0 ]; then
		echo "$PACKAGE_NAME is not installed. Installing..."		
		sudo apt install $PACKAGE_NAME -y
	fi
}

check_if_installed ffmpeg

echo "installing to /usr/local/bin"
sudo cp $FILE /usr/local/bin

# Install to nemo (file manager in Linux Mint) so we can rightclick to transcode files
if [ -e $NEMO_PATH ]; then
    echo "installing to nemo script folder so we can rightclick and transcode."
    cp $FILE $NEMO_PATH
fi

