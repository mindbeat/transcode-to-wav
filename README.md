# Transcode to wav

A small bash-script to transcode mp4 files to wav. 
I made it to get audio support from my mp4-files in Davici Resolve Studio on Linux Mint, because the lack of AAC-decoding. 


Run the install script to install the tool. It will install it under /usr/local/bin and in the nemo script folder, if you run Linux Mint. 

$ ./install.sh

If you use Linux Mint, you can rightclick at folders and files, and then choose script->transcode-to-wav.

Usage: transcode-to-wav [files and/or folders to transcode to wav]

If your argument is a folder, it will search for .mp4/.MP4 files in this folder, and transcode them to wav
